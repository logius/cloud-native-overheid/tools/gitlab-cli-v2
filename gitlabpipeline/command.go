package gitlabpipeline

import (
	"log"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"
)

type flags struct {
	dryRun          *bool
	gitlabURL       *string
	retentionPeriod *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		Run: func(cmd *cobra.Command, args []string) {
			cleanPipelines(cmd, &flags)
		},
		Use:   "clean-pipelines",
		Short: "Clean GitLab Pipelines",
		Long:  "This command removes pipelines and job logs older than specified retentionPeriod.",
	}

	flags.gitlabURL = cmd.Flags().String("gitlab_url", "", "URL of GitLab")
	flags.retentionPeriod = cmd.Flags().String("retentionPeriod", "24h", "Retention period of pipelines")
	flags.dryRun = cmd.Flags().Bool("dry-run", false, "Dry run will nog make changes in GitLab")

	cmd.MarkFlagRequired("gitlab_url")

	return cmd
}

func cleanPipelines(cmd *cobra.Command, flags *flags) error {

	gitlabClient := gitlabclient.NewGitLabClient(*flags.gitlabURL)

	if *flags.dryRun {
		log.Printf("DRY_RUN execution")
	}
	retentionPeriod, err := time.ParseDuration(*flags.retentionPeriod)
	if err != nil {
		panic(err)
	}
	log.Printf("Retention period %v", *flags.retentionPeriod)

	pipelineCleaner := PipelineCleaner{
		GitLabClient:    gitlabClient,
		retentionPeriod: retentionPeriod,
		dryRun:          *flags.dryRun,
	}

	pipelineCleaner.cleanPipelines()

	return nil
}
