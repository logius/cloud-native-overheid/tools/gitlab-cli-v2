package gitlabpipeline

import (
	"log"
	"sort"
	"sync"
	"time"

	"github.com/xanzy/go-gitlab"
)

type PipelineCleaner struct {
	GitLabClient    *gitlab.Client
	dryRun          bool
	retentionPeriod time.Duration
}

// CleanupJobs deletes Jobs and artefacts older than the retention period.
func (pipelineCleaner PipelineCleaner) cleanPipelines() {

	allProjects := pipelineCleaner.readProjects()

	log.Printf("Found %d projects\n", len(allProjects))

	keys := make([]string, 0, len(allProjects))
	for k := range allProjects {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, k := range keys {
		pipelineCleaner.cleanProjectPipelines(k, allProjects[k])
	}

}

func (pipelineCleaner PipelineCleaner) readProjects() map[string]int {
	opt := &gitlab.ListProjectsOptions{}
	opt.Page = 1

	allProjects := map[string]int{}

	for opt.Page > 0 {
		gitlabProjects, r, err := pipelineCleaner.GitLabClient.Projects.ListProjects(opt)
		opt.Page = r.NextPage

		if err != nil {
			if r.StatusCode == 403 {
				log.Printf("Access denied reading projects %v", r.StatusCode)
			} else {
				panic(err)
			}
		}

		for _, project := range gitlabProjects {
			if !project.Archived {
				allProjects[project.PathWithNamespace] = project.ID
			}
		}
	}

	return allProjects
}

func (pipelineCleaner PipelineCleaner) cleanProjectPipelines(projectName string, projectID int) {

	pipelineCounter := 0
	pipelineRemoved := 0

	opt := &gitlab.ListProjectPipelinesOptions{}
	opt.Page = 1
	for opt.Page > 0 {
		pipelines, r, err := pipelineCleaner.GitLabClient.Pipelines.ListProjectPipelines(projectID, opt)
		opt.Page = r.NextPage

		if err != nil {
			if r.StatusCode == 403 {
				log.Printf("Access denied listing pipelines for project %s with statuscode %d", projectName, r.StatusCode)
			} else {
				panic(err)
			}
		}

		for _, pipeline := range pipelines {
			pipelineCounter++
			if time.Since(*pipeline.CreatedAt) > pipelineCleaner.retentionPeriod {
				pipelineCleaner.removePipeline(projectName, projectID, pipeline)
				pipelineRemoved++
			}
		}
	}

	if pipelineCounter > 0 {
		pipelineCleaner.printLog("Project %s has %d pipelines, %d have been removed", projectName, pipelineCounter, pipelineRemoved)
	}
}

func (pipelineCleaner PipelineCleaner) removePipeline(projectName string, projectID int, pipeline *gitlab.PipelineInfo) {

	// First erase the jobs, because only erase job will remove artefacts.
	opt := &gitlab.ListJobsOptions{}
	opt.Page = 1
	for opt.Page > 0 {
		jobs, r, err := pipelineCleaner.GitLabClient.Jobs.ListPipelineJobs(projectID, pipeline.ID, opt)
		opt.Page = r.NextPage

		if err != nil {
			if r.StatusCode != 403 {
				panic(err)
			}
		}

		var wg sync.WaitGroup
		for _, job := range jobs {
			if !pipelineCleaner.dryRun {
				wg.Add(1)
				go func(projectID int, jobID int) {
					defer wg.Done()
					_, r, errJob := pipelineCleaner.GitLabClient.Jobs.EraseJob(projectID, jobID)
					if errJob != nil {

						if r.StatusCode != 403 {
							panic(errJob)
						}
					}
				}(projectID, job.ID)
			}
		}
		wg.Wait()
	}

	// Finally delete Pipeline
	pipelineCleaner.printLog("Remove pipeline %d from project %s created at %v", pipeline.ID, projectName, pipeline.CreatedAt)
	if !pipelineCleaner.dryRun {
		r, err := pipelineCleaner.GitLabClient.Pipelines.DeletePipeline(projectID, pipeline.ID)
		if err != nil {
			if r.StatusCode == 403 {
				log.Printf("--- Access denied deleting pipeline %v in project %s with statuscode %d", pipeline.ID, projectName, r.StatusCode)
			} else {
				panic(err)
			}
		}
	}
}

func (pipelineCleaner PipelineCleaner) printLog(format string, v ...interface{}) {
	if pipelineCleaner.dryRun {
		log.Printf("[ DRY RUN ] "+format, v...)
	} else {
		log.Printf(format, v...)
	}
}
