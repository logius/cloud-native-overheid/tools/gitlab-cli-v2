package gitlabusercleanup

import (
	"fmt"
	"os"
	"time"

	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/k8s"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/keycloak"
)

type flags struct {
	gitlabURL   *string
	keycloakURL *string
	oidcRealm   *string
	dryRun      *bool
}

// Init Variables
var daysAfterUserCreation = 7
var inactivityPeriod = 90

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return cleanupUsersCommand(cmd, &flags)
		},
		Use:   "cleanup-users",
		Short: "Cleanup removed and inactive users",
		Long:  "This command blocks/deactivates users that have been inactive or have been removed from Keycloak.",
	}

	flags.gitlabURL = cmd.Flags().String("gitlab-url", "", "URL of GitLab")
	flags.keycloakURL = cmd.Flags().String("keycloak-url", "", "URL of KeyCloak")
	flags.oidcRealm = cmd.Flags().String("realm", "", "OIDC realm")
	flags.dryRun = cmd.Flags().Bool("dry", false, "Dry Run: Set to true if you want a dry run")

	cmd.MarkFlagRequired("realm")
	cmd.MarkFlagRequired("gitlab-url")
	cmd.MarkFlagRequired("keycloak-url")

	return cmd
}

func cleanupUsersCommand(cmd *cobra.Command, flags *flags) error {
	gitlabClient := gitlabclient.NewGitLabClient(*flags.gitlabURL)

	username, password := k8s.GetKeycloakSecrets()
	os.Setenv("KEYCLOAK_USERNAME", username)
	os.Setenv("KEYCLOAK_PASSWORD", password)

	keyCloakClient, err := keycloak.NewKeyCloakClient(*flags.keycloakURL, *flags.oidcRealm)
	if err != nil {
		return err
	}

	gitlabUsers, err := getGitlabUsers(gitlabClient)
	if err != nil {
		return err
	}

	usersRemoved, err := cleanUpUsers(gitlabClient, keyCloakClient, gitlabUsers, *flags.dryRun)
	if err != nil {
		return err
	}

	fmt.Printf("Cleaned up users! Total Deactivated: %v, Total Blocked: %v", len(usersRemoved["deactivated"]), len(usersRemoved["blocked"]))

	return nil
}

func cleanUpUsers(gitlabClient *gitlab.Client, keycloakClient *keycloak.Client, gitlabUsers []*gitlab.User, dryRun bool) (map[string][]string, error) {
	usersRemoved := make(map[string][]string)

	for _, user := range gitlabUsers {
		if !user.UsingLicenseSeat {
			continue
		}
		if isBuiltinUser(user.Username) {
			fmt.Printf("User %v will be skipped\n", user.Username)
			continue
		}

		fmt.Printf("Checking user %v \n", user.Username)
		userExists, err := keycloakClient.UserExists(user.Username)
		if err != nil {
			return nil, err
		}

		if !userExists && user.State != "blocked" {
			fmt.Printf("User %v has been removed from Keycloak and will be blocked \n", user.Username)
			usersRemoved["blocked"] = append(usersRemoved["blocked"], user.Username)

			if dryRun {
				fmt.Printf("Dry Run, will not block user!\n")
				continue
			}

			//Perform the block
			err := gitlabClient.Users.BlockUser(user.ID)
			if err != nil {
				return nil, err
			}
		}

		//Select Inactive users
		if user.State == "active" {
			deactivate := false
			if userNeverLoggedIn(user) {
				deactivate = true
				fmt.Printf("User %v has not logged in after %v days since creation and will be deactivated\n", user.Username, daysAfterUserCreation)
			} else if userInactive(user) {
				deactivate = true
				fmt.Printf("User %v has been inactive for more than %v days and will be deactivated \n", user.Username, inactivityPeriod)
			}

			if deactivate {
				usersRemoved["deactivated"] = append(usersRemoved["deactivated"], user.Username)

				if dryRun {
					fmt.Printf("Dry Run, will not deactivate user!\n")
					continue
				}

				//Perform the deactivation
				err := gitlabClient.Users.DeactivateUser(user.ID)
				if err != nil {
					return nil, err
				}
			}
		}

	}

	return usersRemoved, nil
}

func userInactive(user *gitlab.User) bool {
	if user.LastActivityOn == nil {
		return true
	}

	t := time.Time(*user.LastActivityOn)
	return int(time.Since(t).Hours()/24) > inactivityPeriod
}

func userNeverLoggedIn(user *gitlab.User) bool {
	return (user.LastActivityOn == nil && int(time.Since(*user.CreatedAt).Hours()/24) > daysAfterUserCreation)
}

var ignoreUsernames = []string{"root", "ghost"}

func isBuiltinUser(username string) bool {
	for _, item := range ignoreUsernames {
		if username == item {
			return true
		}
	}
	return false
}

func getGitlabUsers(gitlabClient *gitlab.Client) ([]*gitlab.User, error) {
	withoutProjectBots := true
	opt := gitlab.ListUsersOptions{
		WithoutProjectBots: &withoutProjectBots,
	}
	opt.Page = 1

	var gitlabUsers = make([]*gitlab.User, 0)
	for opt.Page > 0 {
		users, r, err := gitlabClient.Users.ListUsers(&opt)
		if err != nil {
			return nil, err
		}
		opt.Page = r.NextPage
		gitlabUsers = append(gitlabUsers, users...)
	}
	return gitlabUsers, nil
}
