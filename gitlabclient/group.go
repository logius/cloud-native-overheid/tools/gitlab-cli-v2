package gitlabclient

import (
	"strings"

	"github.com/xanzy/go-gitlab"
)

// GetGitLabGroup gets a group in GitLab
// GitLab search does not support groupnames with 2 chars, so we cannot use the search option
// Search strategy: find the root group and traverse the tree with ListSubgroups()
func GetGitLabGroup(gitLab *gitlab.Client, gitlabGroupPath string) (*gitlab.Group, error) {
	groups := strings.Split(gitlabGroupPath, "/")

	var parentGroup *gitlab.Group
	var err error
	for index, groupName := range groups {
		if index == 0 {
			parentGroup, err = getRootGroup(gitLab, groupName)
			if err != nil {
				return nil, err
			}
		} else {
			parentGroup, err = getSubGroup(gitLab, groupName, parentGroup)
			if err != nil {
				return nil, err
			}
		}
	}
	return parentGroup, nil
}

func getSubGroup(gitLab *gitlab.Client, groupName string, parentGroup *gitlab.Group) (*gitlab.Group, error) {
	opt := &gitlab.ListSubGroupsOptions{}
	opt.Page = 1

	for opt.Page > 0 {
		groups, r, err := gitLab.Groups.ListSubGroups(parentGroup.ID, opt)
		if err != nil {
			return nil, err
		}
		group := getGroup(groups, groupName)
		if group != nil {
			return group, nil
		}
		opt.Page = r.NextPage
	}
	return nil, nil
}

func getRootGroup(gitLab *gitlab.Client, groupName string) (*gitlab.Group, error) {
	topLevelOnly := true
	opt := &gitlab.ListGroupsOptions{TopLevelOnly: &topLevelOnly}
	opt.Page = 1

	for opt.Page > 0 {
		groups, r, err := gitLab.Groups.ListGroups(opt)
		if err != nil {
			return nil, err
		}

		group := getGroup(groups, groupName)
		if group != nil {
			return group, nil
		}
		opt.Page = r.NextPage
	}
	return nil, nil
}

func getGroup(groups []*gitlab.Group, groupName string) *gitlab.Group {
	for _, group := range groups {
		if group.Name == groupName {
			return group
		}
	}
	return nil
}
