package gitlabreport

import (
	"fmt"
	"log"
	"github.com/xanzy/go-gitlab"
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"
)

type flags struct {
	reportType     *string
	gitlabURL   *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return gitlabReport(cmd, &flags)
		},
		Use:   "create-gitlab-report",
		Short: "create gitlab reports",
		Long:  "create gitlab reports, available report types are active-users",
	}

	flags.reportType = cmd.Flags().String("report-type", "", "type of report")
	flags.gitlabURL = cmd.Flags().String("gitlab-url", "", "URL of GitLab")

	cmd.MarkFlagRequired("report-type")
	cmd.MarkFlagRequired("gitlab-url")

	return cmd
}

func gitlabReport(cmd *cobra.Command, flags *flags) error {

	gitlabClient := gitlabclient.NewGitLabClient(*flags.gitlabURL)
    
	reportType := *flags.reportType

	fmt.Println("create report", reportType)

    if reportType == "active-users" {
	getActiveUsers(gitlabClient)
    }
	return nil
}

func getActiveUsers(gitlabClient *gitlab.Client) {
	
    opt := gitlab.ListUsersOptions{}
	opt.Page = 1

	var listActiveUsers = make([]*gitlab.User, 0)
	for opt.Page > 0 {
		users, r, err := gitlabClient.Users.ListUsers(&opt)
		if err != nil {
			log.Fatal(err)
		}
		opt.Page = r.NextPage
		listActiveUsers = append(listActiveUsers, users...)
	}

    count := 0
    for _, user := range listActiveUsers {
     
        if user.UsingLicenseSeat == true {
            count++
        }
        
    }
    fmt.Println("active users", count)

}
