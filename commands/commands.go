package commands

import (
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabgroup"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabpipeline"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabreport"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabusercleanup"
)

// AddMainCommand adds the main command for the current module
func AddMainCommand(rootCmd *cobra.Command) {
	cmd := &cobra.Command{
		Use:   "gitlab",
		Short: "OPS tools for GitLab",
	}
	rootCmd.AddCommand(cmd)

	AddSubCommands(cmd)
}

// AddSubCommands adds subcommands
func AddSubCommands(cmd *cobra.Command) {
	cmd.AddCommand(gitlabgroup.NewCommand())
	cmd.AddCommand(gitlabreport.NewCommand())
	cmd.AddCommand(gitlabusercleanup.NewCommand())
	cmd.AddCommand(gitlabpipeline.NewCommand())

}
