package gitlabgroup

import (
	"log"
	"strings"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"
)

func getGitLabProject(gitLab *gitlab.Client, gitlabProjectPath string) (*gitlab.Project, error) {
	pathElements := strings.Split(gitlabProjectPath, "/")

	group, err := gitlabclient.GetGitLabGroup(gitLab, strings.Join(pathElements[:len(pathElements)-1], "/"))
	if err != nil {
		return nil, err
	}

	opt := gitlab.ListGroupProjectsOptions{}
	opt.PerPage = 500
	projects, _, err := gitLab.Groups.ListGroupProjects(group.ID, &opt)

	if err != nil {
		return nil, err
	}
	for _, project := range projects {
		if project.Name == pathElements[len(pathElements)-1] || project.Path == pathElements[len(pathElements)-1] {
			return project, nil
		}
	}
	return nil, err
}

// Returns member of project, also if the member is inherited by ancestor groups
func GetInheritedProjectMember(gitlabClient *gitlab.Client, gitlabProject *gitlab.Project, userID int) *gitlab.ProjectMember {
	member, _, _ := gitlabClient.ProjectMembers.GetInheritedProjectMember(gitlabProject.ID, userID)
	return member
}

// Returns direct member of project
func getProjectMember(gitlabClient *gitlab.Client, gitlabProject *gitlab.Project, userID int) *gitlab.ProjectMember {
	member, _, _ := gitlabClient.ProjectMembers.GetProjectMember(gitlabProject.ID, userID)
	return member
}

func updateGitLabProjectMember(gitlabClient *gitlab.Client, gitlabProject *gitlab.Project, accessLevel gitlab.AccessLevelValue, userName string, userID int) error {

	opt := gitlab.EditProjectMemberOptions{
		AccessLevel: &accessLevel,
	}

	log.Printf("Update member %q in project %q", userName, gitlabProject.PathWithNamespace)
	_, _, err := gitlabClient.ProjectMembers.EditProjectMember(gitlabProject.ID, userID, &opt)
	return err
}

func createGitLabProjectMember(gitlabClient *gitlab.Client, gitlabProject *gitlab.Project, accessLevel gitlab.AccessLevelValue, userName string, userID int) error {

	opt := gitlab.AddProjectMemberOptions{
		UserID:      &userID,
		AccessLevel: &accessLevel,
	}

	log.Printf("Add member %q in project %q", userName, gitlabProject.PathWithNamespace)
	_, _, err := gitlabClient.ProjectMembers.AddProjectMember(gitlabProject.ID, &opt)
	return err
}
