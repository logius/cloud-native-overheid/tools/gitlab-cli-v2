package gitlabgroup

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/Nerzal/gocloak/v8"
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/k8s"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/keycloak"
)

type flags struct {
	config         *string
	gitlabURL      *string
	keycloakURL    *string
	oidcRealm      *string
	commonGroups   *[]string
	sharedGroups   *[]string
	sharedProjects *[]string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return configureGroups(cmd, &flags)
		},
		Use:   "configure-gitlab-groups",
		Short: "Configure GitLab groups based on KeyCloak groups",
		Long:  "This command configures groups for tenants in GitLab using group information from KeyCloak.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.gitlabURL = cmd.Flags().String("gitlab-url", "", "URL of GitLab")
	flags.keycloakURL = cmd.Flags().String("keycloak-url", "", "URL of KeyCloak")
	flags.oidcRealm = cmd.Flags().String("realm", "", "OIDC realm")
	flags.commonGroups = cmd.Flags().StringSlice("common-groups", []string{}, "comma separated list of common groups")
	flags.sharedGroups = cmd.Flags().StringSlice("shared-groups", []string{}, "list of groups shared between customers. Users wil not be removed automatically.")
	flags.sharedProjects = cmd.Flags().StringSlice("shared-projects", []string{"logius/open/lpc-config/lpc"}, "list of projects shared between customers. Users wil not be removed automatically.")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("realm")
	cmd.MarkFlagRequired("gitlab-url")
	cmd.MarkFlagRequired("keycloak-url")

	return cmd
}

func configureGroups(cmd *cobra.Command, flags *flags) error {

	gitlabClient := gitlabclient.NewGitLabClient(*flags.gitlabURL)

	customerConfig, err := config.LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}

	log.Printf("Configure GitLab group for %q", customerConfig.Customer.Name)

	username, password := k8s.GetKeycloakSecrets()
	os.Setenv("KEYCLOAK_USERNAME", username)
	os.Setenv("KEYCLOAK_PASSWORD", password)

	keyCloakClient, err := keycloak.NewKeyCloakClient(*flags.keycloakURL, *flags.oidcRealm)
	if err != nil {
		return err
	}

	keycloakGroups := getKeyCloakGroups(customerConfig.Customer.GitLab.Groups, customerConfig.Customer.GitLab.Projects)
	keycloakGroupMembers, err := readKeycloakGroups(keyCloakClient, keycloakGroups)
	if err != nil {
		return err
	}

	keycloakUsers := getKeyCloakUsers(keycloakGroupMembers)

	gitlabUsers := configureGitLabUsers(gitlabClient, keycloakUsers)

	cleanupGroupMembers(gitlabClient, customerConfig.Customer.GitLab.Groups, keycloakGroupMembers, *flags.sharedGroups)
	configureGroupMembers(gitlabClient, customerConfig.Customer.GitLab.Groups, keycloakGroupMembers, gitlabUsers, *flags.commonGroups)

	err = configureProjectMembers(gitlabClient, customerConfig.Customer.GitLab.Projects, keycloakGroupMembers, gitlabUsers, *flags.sharedProjects)

	return err
}

func getKeyCloakGroups(gitlabGroups []config.GitLabGroupConfig, gitlabProjects []config.GitLabProjectConfig) []string {
	keycloakgroups := make([]string, 0)

	for _, gitlabGroupConfig := range gitlabGroups {
		for _, rolebinding := range gitlabGroupConfig.Rolebindings {
			for _, keycloakGroupName := range rolebinding.Groups {
				keycloakgroups = append(keycloakgroups, keycloakGroupName)
			}
		}
	}
	for _, gitlabProjectConfig := range gitlabProjects {
		for _, rolebinding := range gitlabProjectConfig.Rolebindings {
			for _, keycloakGroupName := range rolebinding.Groups {
				keycloakgroups = append(keycloakgroups, keycloakGroupName)
			}
		}
	}
	return keycloakgroups
}

func readKeycloakGroups(keyCloakClient *keycloak.Client, groups []string) (map[string][]*gocloak.User, error) {

	keycloakGroupMembers := make(map[string][]*gocloak.User)
	for _, group := range groups {

		groupID, err := keyCloakClient.GetGroupID(group)
		if err != nil {
			return nil, err
		}
		if groupID == "" {
			return nil, fmt.Errorf("could not find group %q in KeyCloak", group)
		}
		members, err := keyCloakClient.GetGroupMembers(groupID)
		if err != nil {
			return nil, err
		}
		keycloakGroupMembers[group] = getProperConfiguredUsers(members)

		log.Printf("KeyCloak group %q has %d members", group, len(keycloakGroupMembers[group]))
	}
	return keycloakGroupMembers, nil
}

func getProperConfiguredUsers(users []*gocloak.User) []*gocloak.User {
	properUsers := make([]*gocloak.User, 0)
	for _, user := range users {
		if strings.HasPrefix(*user.Username, "_") {
			log.Printf("Skip internal user %q", *user.Username)
		} else {
			properUsers = append(properUsers, user)
		}
	}
	return properUsers
}

func getKeyCloakUsers(keycloakGroupMembers map[string][]*gocloak.User) map[string]*gocloak.User {
	keycloakUsers := make(map[string]*gocloak.User)
	for _, members := range keycloakGroupMembers {
		for _, member := range members {
			keycloakUsers[*member.Username] = member
		}
	}
	return keycloakUsers
}
