package gitlabgroup

import (
	"log"
	"strings"

	"github.com/Nerzal/gocloak/v8"
	"github.com/sethvargo/go-password/password"
	"github.com/xanzy/go-gitlab"
)

// ConfigureGitLabUsers creates or updates users in Gitlab using info from KeyCloak.
func configureGitLabUsers(gitlabClient *gitlab.Client, keyCloakUsers map[string]*gocloak.User) map[string]int {

	gitlabUsers := make(map[string]int)
	for _, user := range keyCloakUsers {
		user := configureGitLabUser(gitlabClient, user)
		gitlabUsers[user.Username] = user.ID
	}
	return gitlabUsers
}

func findGitLabUser(gitlabClient *gitlab.Client, username string) *gitlab.User {
	log.Printf("Get user %q", username)

	opt := gitlab.ListUsersOptions{
		Username: &username,
	}
	users, _, err := gitlabClient.Users.ListUsers(&opt)
	if err != nil {
		panic(err)
	}
	if len(users) == 0 {
		return nil
	}
	return users[0]
}

func updateGitLabUser(gitlabClient *gitlab.Client, keyCloakUser *gocloak.User, gitlabUser *gitlab.User) {
	log.Printf("Update GitLab user %q", gitlabUser.Username)

	provider := "keycloak"

	opt := gitlab.ModifyUserOptions{
		Provider:  &provider,
		ExternUID: keyCloakUser.Username,
	}

	_, _, err := gitlabClient.Users.ModifyUser(gitlabUser.ID, &opt)
	if err != nil {
		panic(err)
	}

	if strings.ToLower(gitlabUser.State) == "blocked" {
		err := gitlabClient.Users.UnblockUser(gitlabUser.ID)
		if err != nil {
			panic(err)
		}
	}
}

func configureGitLabUser(gitlabClient *gitlab.Client, keyCloakUser *gocloak.User) *gitlab.User {
	gitlabUser := findGitLabUser(gitlabClient, *keyCloakUser.Username)
	if gitlabUser != nil {
		updateGitLabUser(gitlabClient, keyCloakUser, gitlabUser)
		return gitlabUser
	}

	fullName := *keyCloakUser.FirstName + " " + *keyCloakUser.LastName
	gitlabUsername := strings.ReplaceAll(*keyCloakUser.Username, "@", "")
	external := true
	provider := "keycloak"
	skipConfirmation := true
	resetPassword := false
	randomPassword, _ := password.Generate(64, 10, 10, false, false)

	opt := gitlab.CreateUserOptions{
		Username:         &gitlabUsername,
		Email:            keyCloakUser.Email,
		Name:             &fullName,
		External:         &external,
		Provider:         &provider,
		ExternUID:        keyCloakUser.Username,
		SkipConfirmation: &skipConfirmation,
		Password:         &randomPassword,
		ResetPassword:    &resetPassword,
	}

	log.Printf("Create GitLab user %q", *keyCloakUser.Username)
	gitlabUser, _, err := gitlabClient.Users.CreateUser(&opt)
	if err != nil {
		panic(err)
	}

	// New users are always created in deactivated mode to reduce license cost
	err = gitlabClient.Users.DeactivateUser(gitlabUser.ID)
	if err != nil {
		panic(err)
	}
	return gitlabUser
}
