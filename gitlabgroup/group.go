package gitlabgroup

import (
	"log"
	"strings"

	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/config"

	"github.com/xanzy/go-gitlab"
)

// ConfigureGitLabGroup configures a group in GitLab
func ConfigureGitLabGroup(gitLab *gitlab.Client, gitlabGroup config.GitLabGroupConfig) *gitlab.Group {

	groups := strings.Split(gitlabGroup.Name, "/")

	var parentGroup *gitlab.Group
	for index, groupName := range groups {
		if index == 0 {
			parentGroup = createRootGroup(gitLab, groupName)
		} else {
			parentGroup = createGroup(gitLab, groupName, parentGroup)
		}
	}
	return parentGroup
}

// GitLab search does not support groupnames with 2 chars, so we cannot use the search option
func getSubGroup(gitLab *gitlab.Client, groupName string, parentGroup *gitlab.Group) *gitlab.Group {
	opt := &gitlab.ListSubGroupsOptions{}
	opt.Page = 1

	for opt.Page > 0 {
		groups, r, err := gitLab.Groups.ListSubGroups(parentGroup.ID, opt)
		if err != nil {
			log.Fatal(err)
		}
		opt.Page = r.NextPage
		group := getGroup(groups, groupName)
		if group != nil {
			return group
		}
	}
	return nil
}

func getRootGroup(gitLab *gitlab.Client, groupName string) *gitlab.Group {
	topLevelOnly := true
	opt := &gitlab.ListGroupsOptions{TopLevelOnly: &topLevelOnly}
	opt.Page = 1

	for opt.Page > 0 {
		groups, r, err := gitLab.Groups.ListGroups(opt)
		if err != nil {
			log.Fatal(err)
		}
		opt.Page = r.NextPage
		group := getGroup(groups, groupName)
		if group != nil {
			return group
		}
	}
	return nil
}

func createGroup(gitLab *gitlab.Client, groupName string, parentGroup *gitlab.Group) *gitlab.Group {

	group := getSubGroup(gitLab, groupName, parentGroup)
	if group != nil {
		return group
	}
	visibility := gitlab.PrivateVisibility
	createOpt := &gitlab.CreateGroupOptions{
		Name:       &groupName,
		Visibility: &visibility,
		Path:       &groupName,
		ParentID:   &parentGroup.ID,
	}
	log.Printf("Create child group %q", groupName)
	group, _, err := gitLab.Groups.CreateGroup(createOpt)
	if err != nil {
		panic(err)
	}
	return group
}

func createRootGroup(gitLab *gitlab.Client, rootGroupName string) *gitlab.Group {
	group := getRootGroup(gitLab, rootGroupName)
	if group != nil {
		return group
	}

	visibility := gitlab.PrivateVisibility
	createOpt := &gitlab.CreateGroupOptions{
		Name:       &rootGroupName,
		Visibility: &visibility,
		Path:       &rootGroupName,
	}
	log.Printf("Create root group %q", rootGroupName)
	group, _, err := gitLab.Groups.CreateGroup(createOpt)
	if err != nil {
		panic(err)
	}
	return group
}

func getGroup(groups []*gitlab.Group, groupName string) *gitlab.Group {
	for _, group := range groups {
		if group.Name == groupName || group.Path == groupName {
			return group
		}
	}
	return nil
}
