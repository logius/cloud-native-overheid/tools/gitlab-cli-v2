package gitlabgroup

import (
	"log"
	"regexp"

	"github.com/Nerzal/gocloak/v8"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"
	"k8s.io/utils/strings/slices"
)

var gitlabRoles = map[string]gitlab.AccessLevelValue{
	"owner":      gitlab.OwnerPermission,
	"maintainer": gitlab.MaintainerPermissions,
	"developer":  gitlab.DeveloperPermissions,
	"reporter":   gitlab.ReporterPermissions,
	"guest":      gitlab.GuestPermissions,
}

// Add members if the members is not already a member of an ancestor group with a higher acceslevel
// Delete members
func configureProjectMembers(gitlabClient *gitlab.Client, gitlabProjects []config.GitLabProjectConfig, keycloakGroupMembers map[string][]*gocloak.User, gitlabUsers map[string]int, sharedProjects []string) error {

	for _, gitlabProjectConfig := range gitlabProjects {
		updateGitlabGroupMembers := make(map[string]gitlab.AccessLevelValue)
		newGitlabGroupMembers := make(map[string]gitlab.AccessLevelValue)
		targetGitlabGroupMembers := make(map[string]int)
		gitlabProject, err := getGitLabProject(gitlabClient, gitlabProjectConfig.Name)
		if err != nil {
			return err
		}
		log.Printf("Configure project %q", gitlabProject.PathWithNamespace)

		currentMembersInherited := getExistingGitLabProjectMembersInherited(gitlabClient, gitlabProject)

		for _, rolebinding := range gitlabProjectConfig.Rolebindings {
			for _, keycloakGroupName := range rolebinding.Groups {
				for _, member := range keycloakGroupMembers[keycloakGroupName] {
					found := false
					for _, currentMember := range currentMembersInherited {
						if currentMember.Username != *member.Username {
							continue
						}
						found = true

						if targetAccessLevel, ok := updateGitlabGroupMembers[currentMember.Username]; ok {
							if gitlabRoles[rolebinding.Name] > targetAccessLevel {
								updateGitlabGroupMembers[currentMember.Username] = gitlabRoles[rolebinding.Name]
							}
						} else {
							updateGitlabGroupMembers[currentMember.Username] = gitlabRoles[rolebinding.Name]
						}

						targetGitlabGroupMembers[currentMember.Username] = currentMember.ID
						break

					}
					if !found {
						if currentAccessLevel, ok := newGitlabGroupMembers[*member.Username]; ok {
							if currentAccessLevel < gitlabRoles[rolebinding.Name] {
								newGitlabGroupMembers[*member.Username] = gitlabRoles[rolebinding.Name]
							}
						} else {
							newGitlabGroupMembers[*member.Username] = gitlabRoles[rolebinding.Name]
						}
					}

				}
			}
		}

		for userName, accessLevel := range updateGitlabGroupMembers {
			projectember := getProjectMember(gitlabClient, gitlabProject, gitlabUsers[userName])
			accessLevelAncestorGroup := gitlab.NoPermissions
			if projectember == nil {
				for _, inheritedMember := range currentMembersInherited {
					if inheritedMember.Username == userName {
						accessLevelAncestorGroup = inheritedMember.AccessLevel
					}
				}
			}

			if accessLevelAncestorGroup > accessLevel {
				log.Printf("User %s already has higher acceslevel from ancestor group.", userName)
				continue
			}

			if projectember != nil {
				if projectember.AccessLevel != accessLevel {
					updateGitLabProjectMember(gitlabClient, gitlabProject, accessLevel, userName, gitlabUsers[userName])
				}
			} else {
				createGitLabProjectMember(gitlabClient, gitlabProject, accessLevel, userName, gitlabUsers[userName])
			}
		}
		for userName, accessLevel := range newGitlabGroupMembers {
			createGitLabProjectMember(gitlabClient, gitlabProject, accessLevel, userName, gitlabUsers[userName])
		}

		if !slices.Contains(sharedProjects, gitlabProject.PathWithNamespace) {

			currentMmembers := getExistingGitLabProjectMembers(gitlabClient, gitlabProject)
			for _, currentMember := range currentMmembers {
				if _, ok := targetGitlabGroupMembers[currentMember.Username]; !ok {
					log.Printf("delete  %q from %s", currentMember.Name, gitlabProject.PathWithNamespace)
					gitlabClient.ProjectMembers.DeleteProjectMember(gitlabProject.ID, currentMember.ID)
				}
			}
		}
	}
	return nil
}

// Add members if the members is not already a member of an ancestor group with a higher acceslevel
func configureGroupMembers(gitlabClient *gitlab.Client, gitlabGroups []config.GitLabGroupConfig, keycloakGroupMembers map[string][]*gocloak.User, gitlabUsers map[string]int, commonGroups []string) {

	for _, cg := range commonGroups {

		commonGroup, err := gitlabclient.GetGitLabGroup(gitlabClient, cg)
		if err != nil {
			log.Fatal(err)
		}

		for userName, userId := range gitlabUsers {
			groupMember := getGroupMember(gitlabClient, commonGroup, userId)
			if groupMember == nil {
				accessLevel := gitlab.ReporterPermissions
				createGitLabMember(gitlabClient, commonGroup, accessLevel, userName, userId)
			}
		}
	}

	for _, gitlabGroupConfig := range gitlabGroups {
		targetGitlabGroupMembers := make(map[string]gitlab.AccessLevelValue)
		newGitlabGroupMembers := make(map[string]gitlab.AccessLevelValue)

		gitlabGroup := ConfigureGitLabGroup(gitlabClient, gitlabGroupConfig)
		log.Printf("Configure group %q", gitlabGroup.FullPath)
		currentMembersInherited := getExistingGitLabGroupMembersInherited(gitlabClient, gitlabGroup)

		for _, rolebinding := range gitlabGroupConfig.Rolebindings {
			for _, keycloakGroupName := range rolebinding.Groups {
				for _, member := range keycloakGroupMembers[keycloakGroupName] {
					found := false
					for _, currentMember := range currentMembersInherited {
						if currentMember.Username != *member.Username {
							continue
						}
						found = true
						if targetAccessLevel, ok := targetGitlabGroupMembers[currentMember.Username]; ok {
							if gitlabRoles[rolebinding.Name] > targetAccessLevel {
								targetGitlabGroupMembers[currentMember.Username] = gitlabRoles[rolebinding.Name]
							}
						} else {
							targetGitlabGroupMembers[currentMember.Username] = gitlabRoles[rolebinding.Name]
						}

						break
					}
					if !found {
						if currentAccessLevel, ok := newGitlabGroupMembers[*member.Username]; ok {
							if currentAccessLevel < gitlabRoles[rolebinding.Name] {
								newGitlabGroupMembers[*member.Username] = gitlabRoles[rolebinding.Name]
							}
						} else {
							newGitlabGroupMembers[*member.Username] = gitlabRoles[rolebinding.Name]
						}
					}
				}
			}
		}

		for userName, accessLevel := range targetGitlabGroupMembers {
			groupMember := getGroupMember(gitlabClient, gitlabGroup, gitlabUsers[userName])
			accessLevelAncestorGroup := gitlab.NoPermissions
			if groupMember == nil {
				for _, inheritedMember := range currentMembersInherited {
					if inheritedMember.Username == userName {
						accessLevelAncestorGroup = inheritedMember.AccessLevel
					}
				}
			}

			if accessLevelAncestorGroup > accessLevel {
				log.Printf("User %s already has higher acceslevel from ancestor group.", userName)
				continue
			}

			if groupMember != nil {
				if groupMember.AccessLevel != accessLevel {
					updateGitLabMember(gitlabClient, gitlabGroup, accessLevel, userName, gitlabUsers[userName])
				}
			} else {
				createGitLabMember(gitlabClient, gitlabGroup, accessLevel, userName, gitlabUsers[userName])
			}
		}
		for userName, accessLevel := range newGitlabGroupMembers {
			createGitLabMember(gitlabClient, gitlabGroup, accessLevel, userName, gitlabUsers[userName])
		}
	}
}

func cleanupGroupMembers(gitlabClient *gitlab.Client, gitlabGroups []config.GitLabGroupConfig, keycloakGroupMembers map[string][]*gocloak.User, sharedGroups []string) {
	for _, gitlabGroupConfig := range gitlabGroups {
		gitlabGroup := ConfigureGitLabGroup(gitlabClient, gitlabGroupConfig)
		groupMemberList := getExistingGitLabGroupMembers(gitlabClient, gitlabGroup)
		if !slices.Contains(sharedGroups, gitlabGroup.FullPath) {
			for _, groupMember := range groupMemberList {
				targetAccessLevel := gitlab.NoPermissions
				for _, roleBindings := range gitlabGroupConfig.Rolebindings {

					for _, keycloakGroupName := range roleBindings.Groups {
						for _, keycloakUser := range keycloakGroupMembers[keycloakGroupName] {
							if *keycloakUser.Username == groupMember.Username {

								if gitlabRoles[roleBindings.Name] >= targetAccessLevel {
									targetAccessLevel = gitlabRoles[roleBindings.Name]
								}
								break
							}
						}
					}
				}
				if targetAccessLevel < groupMember.AccessLevel {
					removeGitLabMember(gitlabClient, gitlabGroup, groupMember.Username, groupMember.ID)
				}
			}
		}
	}
}

var regexBotName = regexp.MustCompile("[-_]bot")

// Return all members of the project, including members inherited by ancestor groups
func getExistingGitLabProjectMembersInherited(gitlabClient *gitlab.Client, gitlabProject *gitlab.Project) []*gitlab.ProjectMember {
	opt := gitlab.ListProjectMembersOptions{}
	opt.Page = 1
	var allMembers = make([]*gitlab.ProjectMember, 0)
	for opt.Page > 0 {
		members, r, err := gitlabClient.ProjectMembers.ListAllProjectMembers(gitlabProject.ID, &opt)
		if err != nil {
			log.Fatal(err)
		}
		opt.Page = r.NextPage
		for _, member := range members {
			if !regexBotName.MatchString(member.Username) { // Ignore bot members
				allMembers = append(allMembers, member)
			}
		}
	}
	return allMembers
}

// Return all direct members of the project,
func getExistingGitLabProjectMembers(gitlabClient *gitlab.Client, gitlabProject *gitlab.Project) []*gitlab.ProjectMember {
	opt := gitlab.ListProjectMembersOptions{}
	opt.Page = 1
	var allMembers = make([]*gitlab.ProjectMember, 0)
	for opt.Page > 0 {
		members, r, err := gitlabClient.ProjectMembers.ListProjectMembers(gitlabProject.ID, &opt)
		if err != nil {
			log.Fatal(err)
		}
		opt.Page = r.NextPage
		for _, member := range members {
			if !regexBotName.MatchString(member.Username) { // Ignore bot members
				allMembers = append(allMembers, member)
			}
		}
	}
	return allMembers
}

// Return all members of the group, including members inherited by ancestor groups
func getExistingGitLabGroupMembersInherited(gitlabClient *gitlab.Client, gitlabGroup *gitlab.Group) []*gitlab.GroupMember {
	opt := gitlab.ListGroupMembersOptions{}
	opt.Page = 1
	var allMembers = make([]*gitlab.GroupMember, 0)
	for opt.Page > 0 {
		members, r, err := gitlabClient.Groups.ListAllGroupMembers(gitlabGroup.ID, &opt)
		if err != nil {
			log.Fatal(err)
		}
		opt.Page = r.NextPage
		for _, member := range members {
			if !regexBotName.MatchString(member.Username) { // Ignore bot members
				allMembers = append(allMembers, member)
			}
		}
	}
	return allMembers
}

// Return all direct members of the group,
func getExistingGitLabGroupMembers(gitlabClient *gitlab.Client, gitlabGroup *gitlab.Group) []*gitlab.GroupMember {
	opt := gitlab.ListGroupMembersOptions{}
	opt.Page = 1
	var allMembers = make([]*gitlab.GroupMember, 0)
	for opt.Page > 0 {
		members, r, err := gitlabClient.Groups.ListGroupMembers(gitlabGroup.ID, &opt)
		if err != nil {
			log.Fatal(err)
		}
		opt.Page = r.NextPage
		for _, member := range members {
			if !regexBotName.MatchString(member.Username) { // Ignore bot members
				allMembers = append(allMembers, member)
			}
		}
	}
	return allMembers
}

func getGroupMember(gitlabClient *gitlab.Client, gitlabGroup *gitlab.Group, userID int) *gitlab.GroupMember {
	member, _, _ := gitlabClient.GroupMembers.GetGroupMember(gitlabGroup.ID, userID)
	return member
}

func updateGitLabMember(gitlabClient *gitlab.Client, gitlabGroup *gitlab.Group, accessLevel gitlab.AccessLevelValue, userName string, userID int) {

	opt := gitlab.EditGroupMemberOptions{
		AccessLevel: &accessLevel,
	}

	log.Printf("Update member %q in group %q", userName, gitlabGroup.FullPath)
	_, _, err := gitlabClient.GroupMembers.EditGroupMember(gitlabGroup.ID, userID, &opt)
	if err != nil {
		log.Fatal(err)
	}
}

func createGitLabMember(gitlabClient *gitlab.Client, gitlabGroup *gitlab.Group, accessLevel gitlab.AccessLevelValue, userName string, userID int) {

	opt := gitlab.AddGroupMemberOptions{
		UserID:      &userID,
		AccessLevel: &accessLevel,
	}

	log.Printf("Add member %q in group %q", userName, gitlabGroup.FullPath)
	_, _, err := gitlabClient.GroupMembers.AddGroupMember(gitlabGroup.ID, &opt)
	if err != nil {
		log.Fatal(err)
	}
}

func removeGitLabMember(gitlabClient *gitlab.Client, gitlabGroup *gitlab.Group, userName string, userID int) {

	opt := gitlab.RemoveGroupMemberOptions{}
	log.Printf("Remove member %q from group %q", userName, gitlabGroup.FullPath)
	_, err := gitlabClient.GroupMembers.RemoveGroupMember(gitlabGroup.ID, userID, &opt)
	if err != nil {
		log.Fatal(err)
	}
}

func getUserID(gitlabUsers map[string]int, userName string) int {
	return gitlabUsers[userName]
}

func getGitLabUserName(gitlabUsers map[string]int, userID int) string {
	for username, gitlabUserID := range gitlabUsers {
		if userID == gitlabUserID {
			return username
		}
	}
	return ""
}
