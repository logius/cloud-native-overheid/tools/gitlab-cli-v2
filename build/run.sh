#!/bin/bash

TOOLBOX_IMG=registry.gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli:local

export GITLAB_ACCESS_TOKEN="<redacted>"

docker run --rm \
  -e GITLAB_ACCESS_TOKEN \
  $TOOLBOX_IMG clean-pipelines --url=https://gitlab.l12m.nl --retention=2160h
