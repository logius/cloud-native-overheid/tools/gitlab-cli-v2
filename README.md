# gitlab-cli


## Config key: customer.gitlab.groups.rolemapping
Use `customer.groups` as rolemapping for gitlab users.
Define user groups under customer.groups with a name and its members.
Members can be identified by username or email
For example:
```yaml
customer:
  name: lpcaccesstest
  groups:
    - name: lpcaccesstest_teamlead
      members:
        - tester1
    - name: lpcaccesstest_maintainer
      members:
        - tester2
```
The groups can be referred to as role mapping for gitlab groups.
```yaml
  gitlab:
    groups:
    - name: logius/closed/lpcaccesstest
      rolemappings:
        owner:
        - lpcaccesstest_teamlead
        maintainer:
        - lpcaccesstest_maintainer
        developer:
        - lpcaccesstest_developer
```
