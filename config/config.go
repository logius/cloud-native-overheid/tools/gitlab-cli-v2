package config

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v3"
)

// Rolebinding defines the binding between role and zero or more groups
type Rolebinding struct {
	Name   string   `validate:"required"`
	Groups []string `validate:"required" yaml:"groups,omitempty"`
}

// GitLabProjectConfig spec
type GitLabProjectConfig struct {
	Name         string
	Rolebindings []Rolebinding `yaml:"roles"`
}

// GitLabGroupConfig spec
type GitLabGroupConfig struct {
	Name         string        `validate:"required"`
	Rolebindings []Rolebinding `validate:"required,dive" yaml:"roles"`
}

// GitLab configuration
type GitLab struct {
	Groups   []GitLabGroupConfig `validate:"required,dive"`
	Projects []GitLabProjectConfig
}

// GroupConfig spec
type GroupConfig struct {
	Name string `validate:"required" yaml:"name"`
}

// Customer configuration
type Customer struct {
	Name   string `validate:"required"`
	GitLab GitLab `validate:"required"`
}

// Config contains customer configuration
type Config struct {
	Customer Customer `validate:"required"`
}

// LoadConfigfile reads config from YAML
func LoadConfigfile(fileName string) (*Config, error) {

	var config Config
	if err := readConfig(fileName, &config); err != nil {
		return nil, err
	}

	return &config, nil
}

func readConfig(fileName string, config interface{}) error {
	yamlFile, err := ioutil.ReadFile(fileName)
	if err != nil {
		return fmt.Errorf("error reading YAML file: %s\n", err)
	}

	err = yaml.Unmarshal(yamlFile, config)
	if err != nil {
		return fmt.Errorf("error parsing YAML file: %s\n", err)
	}
	return nil
}
